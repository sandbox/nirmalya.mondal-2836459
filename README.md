Crosswords
======================
Crosswords module creates and displays crosswords.

 - A predefined Locked content type "crosswords" get created 
   i.e. similar to other Drupal Content Types.
 - Site Admin or Content Editor can add Crosswords through this content type.
 - Site Admin can set which Crosswords to display with link like this
   http://mysite.com/crosswords/2/ where 2 is nid of the Crosswords.
 - To display Crosswords in frontend please check the url 
   http://mysite.com/crosswords/2/  instead of {node} please use {crosswords}.
 - There is a Check Crosswords button appears in frontend to validate answers.
 - Adding and Removing of Hints/ Clues can be configured very easily.
 - Site editor can set the re-size of the Crosswords box 
   and can place the words inside the box.
 - Uncheck the PROMOTION OPTIONS for not promoting to front page 
   as this plugin render Crosswords differently.
 - Un-installation automatically deleted the Crosswords contents 
   and content type.
 - External Javascript library Handsontable has been used.
   Instead of CDN user can download and use it from here 
   cdnjs.cloudflare.com/ajax/libs/handsontable/0.30.1/handsontable.full.js
 - External CSS file has been used from Handsontable.
   Instead of CDN user can download and use it from here 
   cdnjs.cloudflare.com/ajax/libs/handsontable/0.30.1/handsontable.full.min.css

This module is safe to use on a production site. 

Author/ Maintainers
======================
- Nirmalya Mondal <nirmalya.mondal@gmail.com> http://nirmalyamondal.blogspot.in/
