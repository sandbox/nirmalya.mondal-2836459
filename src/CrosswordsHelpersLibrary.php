<?php

namespace Drupal\crosswords;

/**
 * @file
 * Contains \Drupal\crosswords\CrosswordsHelpersLibrary.
 */

/**
 * Returns responses for various Helper Functions.
 */
class CrosswordsHelpersLibrary {

  /**
   * {@inheritdoc}
   */
  public static function processCrosswordsHints($savedCrosswordsHint) {
    // Processing Crosswords Row HINT.
    if (strpos($savedCrosswordsHint, '###RANDC###') !== FALSE) {
      $explodedSavedCrosswordsHint = explode('###RANDC###', $savedCrosswordsHint);
    }
    $savedCrosswordsHintRow['row'] = '[' . $explodedSavedCrosswordsHint[0] . ']';
    $savedCrosswordsHintRow['col'] = '[' . $explodedSavedCrosswordsHint[1] . ']';
    if (strpos($explodedSavedCrosswordsHint[2], '###') !== FALSE) {
      $explodedSavedCrosswordsHintInputArray = explode('###', $explodedSavedCrosswordsHint[2]);
      $explodedSavedCrosswordsHintInputString = implode('","', $explodedSavedCrosswordsHintInputArray);
    }
    $savedCrosswordsHintRow['data'] = '["' . $explodedSavedCrosswordsHintInputString . '"]';
    return $savedCrosswordsHintRow;
  }

}
